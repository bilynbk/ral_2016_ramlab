README for the multimedia attachment of the paper "The Significance of Robotic Gait Selection: 
A Case Study on the Robot RAMone" submitted to Robotics and Automation Letters 2016.

Minimum requirements for the user:

    Matlab R2015b or higher (needs symbolic math toolbox)


Any questions can be sent to Nils Smit-Anseeuw at nilssmit@umich.edu
